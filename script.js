const person = {
    firstName: "Robert",
    lastName: "Smith",
    age: 35,
    height: 170,
    weight: 60,
    address: {
        country: "Georia",
        city: "Tbilisi",
        street: "Kvarackheli",
        house: 36,
    },
    body: {
        anchor: {
            leaflet: {
                someValue: "1",
                someValue1: "2",
                additionalObject: {
                    depthLevel: 4,
                    dopObject: {
                        level: 5,
                        level5: 5,
                    },
                },
            },
        },
    },
    "business address": {
        country: "Armenia",
        city: "Erevan",
        street: "Amayak Akopyan",
        house: 25,
    },
};

let result = ["{"];
let n = 1;

function recursion(person, n) {
    for (const [key, value] of Object.entries(person)) {
        if (typeof value !== "object") {
            result.push("  ".repeat(n) + `${key}: ${value}`);
        } else {
            result.push("  ".repeat(n) + `${key}: {`);

            recursion(value, n + 1);

            result.push("  ".repeat(n) + `}`);
        }
    }
}

recursion(person, n);

result.push("}");

for (let i = 0; i < result.length; i++) {
    console.log(result[i]);
}
alert(result);

